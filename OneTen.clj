(require '[clojure.string :as s])

(defn my-last [xs]
    (last xs))

(defn my-but-last [xs]
    (second (reverse xs)))

(defn element-at [xs k]
    (last (take k xs)))

; Like my Haskell solution that doesn't use zipWith, except more verbose.
(defn my-length [xs]
    (loop [loop-list xs count 0]
        (if-not (empty? loop-list)
            (recur (rest loop-list) (inc count))
            count)))

(defn my-reverse [xs]
    (loop [new-list nil loop-list xs]
        (if-not (empty? loop-list)
            (recur (conj new-list (first loop-list)) (rest loop-list))
            (apply str new-list))))

(defn palindrome? [xs]
    (= xs (s/reverse xs)))

; Just like the Haskell solution
(defn my-flatten [xs]
    (if (seq? xs)
        (apply concat (map my-flatten xs))
        (list xs)))

; Just checking the behavior of collections
(defn my-print [xs]
    (if-not (empty? xs)
        (do
            (println (first xs))
            (recur (rest xs)))
        nil))

; Can't get it to append to the new list in the proper order.
; Did this the stupid way. Uses an extra list. Going to fix it with the second
; Version
(defn my-compress [xs]
    (loop [compressed-list nil loop-list xs current (first xs)]
        (if-not (empty? loop-list)
            (if (= (first loop-list) current)
                (recur compressed-list (rest loop-list) current)
                (recur
                    (cons current compressed-list)
                    (rest loop-list)
                    (first loop-list)))
            (reverse (cons current compressed-list)))))

; Much nicer than the first one
(defn my-compress2 [xs]
    (if-not (empty? xs)
        (if (= (first xs) (second xs))
            (recur (drop 1 xs))
            (cons (first xs) (my-compress2 (rest xs))))))

; Using mapcat is not the same as (apply concat (map f x)).
;Nested structures seem to upset it.
; (defn my-flatten2 [xs]
;     (if (seq? xs)
;         (mapcat my-flatten2 xs))
;         (list xs))

(defn pack [xs]
    (let [current (first xs)]
    (if-not (empty? xs)
        (cons
            (take-while (partial = current) xs)
            (pack (drop-while (partial = current) xs)))
        '())))

; This way works, but has to calculate the number of repeats twice.
; (defn encode [xs]
;     (let [current (first xs)]
;     (if-not (empty? xs)
;         (cons
;             (list (count (take-while (partial = current) xs)) current)
;             (encode (drop-while (partial = current) xs)))
;         '())))

(defn encode [xs]
    (let [current (first xs)]
    (if-not (empty? xs)
        (let [number (count (take-while (partial = current) xs))]
            (cons
                (list number current)
                (encode (drop number xs))))
        nil)))
