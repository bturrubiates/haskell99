import Control.Arrow ((&&&))

myLast :: [a] -> a
myLast = head . reverse

myButLast :: [a] -> a
myButLast = head . tail . reverse

-- Very obvious solution
elementAt :: [a] -> Int -> a
elementAt xs x = last $ take x xs

-- Point free syntax for Tyler
elementAt' :: Int -> [a] -> a
-- elementAt' x = last . (take x)
-- elementAt' xs x = last (take x xs)
elementAt' = (last .) . (take)

-- Original and most obvious solution
myLength :: [a] -> Int
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

-- A stupid way
myLength' :: [a] -> Int
myLength' xs = sum $ zipWith (\_ _ -> 1) xs [1..]


-- Obvious Solution
myReverse :: [a] -> [a]
myReverse [] = []
myReverse (x:xs) = myReverse xs ++ [x]

-- Painfully obvious solution
isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome xs = xs == (reverse xs)

data NestedList a = Elem a | List [NestedList a]

flatten :: NestedList a -> [a]
flatten (Elem x) = [x]
flatten (List x) = concatMap flatten x

-- Derpy
compress :: (Eq a) => [a] -> [a]
compress [] = []
compress (x:xs) = do
    let y = dropWhile (== x) xs
    [x] ++ compress y

-- Derpy way
pack :: (Eq a) => [a] -> [[a]]
pack [] = []
pack (x:xs) = do
    let y = takeWhile (==x) xs
    (x : y) : pack (dropWhile (==x) xs)

-- Same but not syntactically stupid
pack' :: (Eq a) => [a] -> [[a]]
pack' [] = []
pack' (x:xs) = (x : takeWhile (==x) xs) : pack' (dropWhile (==x) xs)

encode :: (Eq a) => [a] -> [(Int, a)]
encode xs = map (length &&& head) (pack' xs)

-- Point free style
encode' :: (Eq a) => [a] -> [(Int, a)]
encode' = map (length &&& head) . pack'